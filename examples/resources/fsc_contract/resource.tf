resource "fsc_contract" "example1" {
  id             = "fec3c317-ea47-4ced-8f09-0a8f0dad29f0"
  hash_algorithm = "SHA3_512"
  revoked        = false

  validity {
    not_before = 1703800000
    not_after  = 1703900000
  }

  grant {
    outway_peer_id                = "01234567891011121314"
    service_peer_id               = "01234567891011121314"
    service_name                  = "foo"
    service_delegator_peer_id     = ""
    type                          = "SERVICE_CONNECTION"
    outway_certificate_thumbprint = "Wun7-JmHS-g9GLo8ZzBVor5RD170XCILu5XDs6X6Lws"
  }

  grant {
    delegator_peer_id             = "01234567891011121314"
    outway_peer_id                = "01234567891011121314"
    service_peer_id               = "01234567891011121314"
    service_name                  = "foo"
    service_delegator_id          = ""
    type                          = "DELEGATED_SERVICE_CONNECTION"
    outway_certificate_thumbprint = "Wun7-JmHS-g9GLo8ZzBVor5RD170XCILu5XDs6X6Lws"
  }
}

resource "fsc_contract" "example2" {
  id             = "fec3c317-ea47-4ced-8f09-0a8f0dad29f1"
  hash_algorithm = "SHA3_512"
  revoked        = false

  validity {
    not_before = 1703800000
    not_after  = 1703900000
  }

  grant {
    directory_peer_id             = "01234567891011121314"
    service_peer_id               = "01234567891011121314"
    service_name                  = "foo"
    service_protocol              = "PROTOCOL_TCP_HTTP_1.1"
    type                          = "SERVICE_PUBLICATION"
    outway_certificate_thumbprint = "Wun7-JmHS-g9GLo8ZzBVor5RD170XCILu5XDs6X6Lws"
  }
}

resource "fsc_contract" "example3" {
  id             = "fec3c317-ea47-4ced-8f09-0a8f0dad29f2"
  hash_algorithm = "SHA3_512"
  revoked        = false

  validity {
    not_before = 1703800000
    not_after  = 1703900000
  }

  grant {
    delegator_peer_id             = "01234567891011121314"
    directory_peer_id             = "01234567891011121314"
    service_peer_id               = "01234567891011121314"
    service_name                  = "foo"
    service_protocol              = "PROTOCOL_TCP_HTTP_1.1"
    type                          = "DELEGATED_SERVICE_PUBLICATION"
    outway_certificate_thumbprint = "Wun7-JmHS-g9GLo8ZzBVor5RD170XCILu5XDs6X6Lws"
  }
}
