// Copyright (c) HashiCorp, Inc.
// SPDX-License-Identifier: MPL-2.0

package provider

import (
	"context"
	"fmt"
	"regexp"
	"strings"

	fsc "terraform-provider-fsc/api"

	"github.com/hashicorp/terraform-plugin-framework-validators/listvalidator"
	"github.com/hashicorp/terraform-plugin-framework-validators/stringvalidator"
	"github.com/hashicorp/terraform-plugin-framework/path"
	"github.com/hashicorp/terraform-plugin-framework/resource"
	"github.com/hashicorp/terraform-plugin-framework/resource/schema"
	"github.com/hashicorp/terraform-plugin-framework/schema/validator"
	"github.com/hashicorp/terraform-plugin-framework/types"
	"github.com/hashicorp/terraform-plugin-framework/types/basetypes"
	"github.com/hashicorp/terraform-plugin-log/tflog"
)

var _ resource.Resource = &ContractResource{}
var _ resource.ResourceWithImportState = &ContractResource{}

func NewContractResource() resource.Resource {
	return &ContractResource{}
}

type ContractResource struct {
	client *fsc.Client
}

type ContractResourceModel struct {
	Iv            types.String `tfsdk:"iv"`
	Validity      types.Object `tfsdk:"validity"`
	Grants        types.List   `tfsdk:"grant"`
	HashAlgorithm types.String `tfsdk:"hash_algorithm"`
	Revoked       types.Bool   `tfsdk:"revoked"`
}

type Validity struct {
	NotBefore types.Int64 `tfsdk:"not_before"`
	NotAfter  types.Int64 `tfsdk:"not_after"`
}

type Grant struct {
	Type                      types.String `tfsdk:"type"`
	DelegatorPeerId           types.String `tfsdk:"delegator_peer_id"`
	DirectoryPeerId           types.String `tfsdk:"directory_peer_id"`
	OutwayPeerId              types.String `tfsdk:"outway_peer_id"`
	OutwayPublicKeyThumbprint types.String `tfsdk:"outway_public_key_thumbprint"`
	ServicePeerId             types.String `tfsdk:"service_peer_id"`
	ServiceName               types.String `tfsdk:"service_name"`
	ServiceProtocol           types.String `tfsdk:"service_protocol"`
	ServiceDelegatorPeerId    types.String `tfsdk:"service_delegator_peer_id"`
}

func (r *ContractResource) Metadata(ctx context.Context, req resource.MetadataRequest, resp *resource.MetadataResponse) {
	resp.TypeName = req.ProviderTypeName + "_contract"
}

func (r *ContractResource) Schema(ctx context.Context, req resource.SchemaRequest, resp *resource.SchemaResponse) {
	resp.Schema = schema.Schema{
		// This description is used by the documentation generator and the language server.
		MarkdownDescription: "Contract resource",

		Attributes: map[string]schema.Attribute{
			"iv": schema.StringAttribute{
				Required:            true,
				MarkdownDescription: "Contract identifier",
				Validators: []validator.String{
					stringvalidator.RegexMatches(
						regexp.MustCompile(
							`^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$`),
						"must be a valid UUID",
					),
				},
			},
			"hash_algorithm": schema.StringAttribute{
				Required:            true,
				MarkdownDescription: "Contract's hashing algorithm",
				Validators: []validator.String{
					stringvalidator.OneOfCaseInsensitive([]string{"SHA3_512"}...),
				},
			},
			"revoked": schema.BoolAttribute{
				Required:            true,
				MarkdownDescription: "The Contract has been revoked",
			},
		},
		Blocks: map[string]schema.Block{
			"validity": schema.SingleNestedBlock{
				Attributes: map[string]schema.Attribute{
					"not_before": schema.Int64Attribute{
						Required:            true,
						MarkdownDescription: "The Contract is not valid before this date",
					},
					"not_after": schema.Int64Attribute{
						Required:            true,
						MarkdownDescription: "The Contract is not valid after this date",
					},
				},
			},
			"grant": schema.ListNestedBlock{ // TODO: improve input validation for individual grant types
				NestedObject: schema.NestedBlockObject{
					Attributes: map[string]schema.Attribute{
						"type": schema.StringAttribute{
							Required:            true,
							MarkdownDescription: "The type of Grant",
							Validators: []validator.String{
								stringvalidator.OneOfCaseInsensitive([]string{
									"SERVICE_PUBLICATION", "SERVICE_CONNECTION",
									"DELEGATED_SERVICE_PUBLICATION", "DELEGATED_SERVICE_CONNECTION",
								}...),
							},
						},
						"delegator_peer_id": schema.StringAttribute{
							Optional:            true,
							MarkdownDescription: "The ID of a Delegator Peer",
							Validators: []validator.String{
								stringvalidator.LengthBetween(20, 20),
								stringvalidator.RegexMatches(
									regexp.MustCompile(
										`^\d{20}$`),
									"must be a valid ID (20 digits)",
								),
							},
						},
						"directory_peer_id": schema.StringAttribute{
							Optional:            true,
							MarkdownDescription: "The ID of a Directory Peer",
							Validators: []validator.String{
								stringvalidator.LengthBetween(20, 20),
								stringvalidator.RegexMatches(
									regexp.MustCompile(
										`^\d{20}$`),
									"must be a valid ID (20 digits)",
								),
							},
						},
						"outway_peer_id": schema.StringAttribute{
							Optional:            true,
							MarkdownDescription: "The ID of a Outway Peer",
							Validators: []validator.String{
								stringvalidator.LengthBetween(20, 20),
								stringvalidator.RegexMatches(
									regexp.MustCompile(
										`^\d{20}$`),
									"must be a valid ID (20 digits)",
								),
							},
						},
						"outway_public_key_thumbprint": schema.StringAttribute{
							Optional:            true,
							MarkdownDescription: "The SHA256 thumbprint of the Public Key of the Certificate of the Outway in hex format",
							Validators: []validator.String{
								stringvalidator.LengthAtLeast(1),
							},
						},
						"service_peer_id": schema.StringAttribute{
							Required:            true,
							MarkdownDescription: "The ID of a Service Peer",
							Validators: []validator.String{
								stringvalidator.LengthBetween(20, 20),
								stringvalidator.RegexMatches(
									regexp.MustCompile(
										`^\d{20}$`),
									"must be a valid ID (20 digits)",
								),
							},
						},
						"service_delegator_peer_id": schema.StringAttribute{
							Optional:            true,
							MarkdownDescription: "The ID of a Service Delegator Peer",
							Validators: []validator.String{
								stringvalidator.LengthBetween(20, 20),
								stringvalidator.RegexMatches(
									regexp.MustCompile(
										`^\d{20}$`),
									"must be a valid ID (20 digits)",
								),
							},
						},
						"service_name": schema.StringAttribute{
							Optional:            true,
							MarkdownDescription: "The name of a Service",
							Validators: []validator.String{
								stringvalidator.RegexMatches(
									regexp.MustCompile(
										`^[a-zA-Z0-9-_.]+$`),
									"must contain only letters, numbers, -, _ and .",
								),
							},
						},
						"service_protocol": schema.StringAttribute{
							Optional:            true,
							MarkdownDescription: "The protocol of a Service",
							Validators: []validator.String{
								stringvalidator.OneOfCaseInsensitive([]string{
									"PROTOCOL_TCP_HTTP_1.1", "PROTOCOL_TCP_HTTP_2",
								}...),
							},
						},
					},
				},
				Validators: []validator.List{
					listvalidator.IsRequired(),
				},
			},
		},
	}
}

func (r *ContractResource) Configure(ctx context.Context, req resource.ConfigureRequest, resp *resource.ConfigureResponse) {
	// Prevent panic if the provider has not been configured.
	if req.ProviderData == nil {
		return
	}

	client, ok := req.ProviderData.(*fsc.Client)

	if !ok {
		resp.Diagnostics.AddError(
			"Unexpected Resource Configure Type",
			fmt.Sprintf("Expected *fsc.Client, got: %T. Please report this issue to the provider developers.", req.ProviderData),
		)

		return
	}

	r.client = client
}

func (r *ContractResource) Create(ctx context.Context, req resource.CreateRequest, resp *resource.CreateResponse) {
	var plan ContractResourceModel

	// Read Terraform plan data into the model
	resp.Diagnostics.Append(req.Plan.Get(ctx, &plan)...)

	if resp.Diagnostics.HasError() {
		return
	}

	var validity Validity
	plan.Validity.As(ctx, &validity, basetypes.ObjectAsOptions{})

	plannedGrants := make([]Grant, 0, len(plan.Grants.Elements()))
	plan.Grants.ElementsAs(ctx, &plannedGrants, false)

	contract, err := r.client.GetContract(plan.Iv.ValueString())
	if err != nil {
		resp.Diagnostics.AddError(
			"Error creating Contract",
			"Could not enumerate contracts: "+err.Error(),
		)
		return
	}

	if contract == nil {
		grants := []fsc.Grant{}
		for _, grant := range plannedGrants {
			grantType := "GRANT_TYPE_" + strings.ToUpper(grant.Type.ValueString())

			switch grantType {
			case fsc.ServicePublicationGrantType:
				g := fsc.GrantServicePublication(
					grant.DirectoryPeerId.ValueString(), grant.ServicePeerId.ValueString(), grant.ServiceName.ValueString(),
					grant.ServiceProtocol.ValueString())
				grants = append(grants, g)
			case fsc.ServiceConnectionGrantType:
				g := fsc.GrantServiceConnection(
					grant.OutwayPublicKeyThumbprint.ValueString(), grant.OutwayPeerId.ValueString(), grant.ServicePeerId.ValueString(),
					grant.ServiceDelegatorPeerId.ValueString(), grant.ServiceName.ValueString())
				grants = append(grants, g)
			case fsc.DelegatedServicePublicationGrantType:
				g := fsc.GrantDelegatedServicePublication(
					grant.DelegatorPeerId.ValueString(), grant.DirectoryPeerId.ValueString(), grant.ServicePeerId.ValueString(),
					grant.ServiceName.ValueString(), grant.ServiceProtocol.ValueString())
				grants = append(grants, g)
			case fsc.DelegatedServiceConnectionGrantType:
				g := fsc.GrantDelegatedServiceConnection(
					grant.DelegatorPeerId.ValueString(), grant.OutwayPublicKeyThumbprint.ValueString(), grant.OutwayPeerId.ValueString(),
					grant.ServicePeerId.ValueString(), grant.ServiceDelegatorPeerId.ValueString(), grant.ServiceName.ValueString())
				grants = append(grants, g)
			}
		}

		err := r.client.CreateContract(plan.Iv.ValueString(), plan.HashAlgorithm.ValueString(), validity.NotBefore.ValueInt64(), validity.NotAfter.ValueInt64(), grants)
		if err != nil {
			resp.Diagnostics.AddError(
				"Error creating Contract",
				"Could not create contract, unexpected error: "+err.Error(),
			)
			return
		}
	}

	tflog.Trace(ctx, "created a resource")

	// Save data into Terraform state
	resp.Diagnostics.Append(resp.State.Set(ctx, &plan)...)
}

func (r *ContractResource) Read(ctx context.Context, req resource.ReadRequest, resp *resource.ReadResponse) {
	var state ContractResourceModel

	// Read Terraform prior state data into the model
	resp.Diagnostics.Append(req.State.Get(ctx, &state)...)

	if resp.Diagnostics.HasError() {
		return
	}

	contract, err := r.client.GetContract(state.Iv.ValueString())
	if err != nil {
		resp.Diagnostics.AddError(
			"Error Reading Contract",
			"Could not read contract: "+err.Error(),
		)
		return
	}

	if contract == nil {
		resp.State.RemoveResource(ctx)
		return
	}

	state.Revoked = types.BoolValue(contract.HasRevoked)

	// Save updated data into Terraform state
	resp.Diagnostics.Append(resp.State.Set(ctx, &state)...)
}

func (r *ContractResource) Update(ctx context.Context, req resource.UpdateRequest, resp *resource.UpdateResponse) {
	var plan, state ContractResourceModel

	// Read Terraform plan data into the model
	resp.Diagnostics.Append(req.Plan.Get(ctx, &plan)...)
	resp.Diagnostics.Append(req.State.Get(ctx, &state)...)

	if resp.Diagnostics.HasError() {
		return
	}

	if state.Revoked.ValueBool() == false && plan.Revoked.ValueBool() == true {
		contract, err := r.client.GetContract(state.Iv.ValueString())
		if err != nil {
			resp.Diagnostics.AddError(
				"Error Reading Contract",
				"Could not read contract: "+err.Error(),
			)
			return
		}

		err = r.client.RevokeContract(contract.Hash)
		if err != nil {
			resp.Diagnostics.AddError(
				"Error revoking Contract",
				"Could not revoke contract: "+err.Error(),
			)
			return
		}
	} else {
		resp.Diagnostics.AddError(
			"FSC Contracts are immutable",
			"It's not possible to update FSC contracts as they are immutable. If the contents of a contract are no longer valid the contract should be revoked and replaced by a new one.",
		)
		return
	}

	// Save updated data into Terraform state
	resp.Diagnostics.Append(resp.State.Set(ctx, &plan)...)
}

func (r *ContractResource) Delete(ctx context.Context, req resource.DeleteRequest, resp *resource.DeleteResponse) {
	// Intentionally unimplemented
}

func (r *ContractResource) ImportState(ctx context.Context, req resource.ImportStateRequest, resp *resource.ImportStateResponse) {
	resource.ImportStatePassthroughID(ctx, path.Root("id"), req, resp)
}

func (r *ContractResource) ModifyPlan(ctx context.Context, req resource.ModifyPlanRequest, resp *resource.ModifyPlanResponse) {
	if req.Plan.Raw.IsNull() {
		resp.Diagnostics.AddError(
			"FSC Contracts are immutable",
			"It's not possible to update FSC contracts as they are immutable. If the contents of a contract are no longer valid the contract should be revoked and replaced by a new one.",
		)
		return
	}
}
