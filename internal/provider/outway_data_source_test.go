// Copyright (c) HashiCorp, Inc.
// SPDX-License-Identifier: MPL-2.0

package provider

import (
	"fmt"
	"regexp"
	"testing"

	"github.com/hashicorp/terraform-plugin-testing/helper/resource"
)

func TestAccOutwayDataSource(t *testing.T) {
	outways, err := apiClient.GetOutways()
	if err != nil || len(outways) == 0 {
		t.Logf("Failed to find any registered Outway")
		t.FailNow()
	}

	outwayName := outways[0].Name
	notEmpty := regexp.MustCompile(`^.+$`)
	isSHA := regexp.MustCompile(`^[a-fA-F0-9]{64}$`)

	resource.Test(t, resource.TestCase{
		PreCheck:                 func() { testAccFscClientCheck(t) },
		ProtoV6ProviderFactories: testAccProtoV6ProviderFactories,
		Steps: []resource.TestStep{
			// Read testing
			{
				Config: testAccOutwayDataResourceConfig(outwayName),
				Check: resource.ComposeAggregateTestCheckFunc(
					resource.TestCheckResourceAttr("data.fsc_outway.foo", "name", outwayName),
					resource.TestMatchResourceAttr("data.fsc_outway.foo", "public_key_thumbprint", isSHA),
					resource.TestMatchResourceAttr("data.fsc_outway.foo", "peer_id", notEmpty),
				),
			},
		},
	})
}

func testAccOutwayDataResourceConfig(name string) string {
	return fmt.Sprintf(`
data "fsc_outway" "foo" {
  name          = %[1]q
}`, name)
}
