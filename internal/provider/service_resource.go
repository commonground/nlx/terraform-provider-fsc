// Copyright (c) HashiCorp, Inc.
// SPDX-License-Identifier: MPL-2.0

package provider

import (
	"context"
	"fmt"
	"regexp"

	"github.com/hashicorp/terraform-plugin-framework-validators/stringvalidator"
	"github.com/hashicorp/terraform-plugin-framework/path"
	"github.com/hashicorp/terraform-plugin-framework/resource"
	"github.com/hashicorp/terraform-plugin-framework/resource/schema"
	"github.com/hashicorp/terraform-plugin-framework/resource/schema/planmodifier"
	"github.com/hashicorp/terraform-plugin-framework/resource/schema/stringplanmodifier"
	"github.com/hashicorp/terraform-plugin-framework/schema/validator"
	"github.com/hashicorp/terraform-plugin-framework/types"
	"github.com/hashicorp/terraform-plugin-log/tflog"
	fsc "terraform-provider-fsc/api"
)

var _ resource.Resource = &ServiceResource{}
var _ resource.ResourceWithImportState = &ServiceResource{}

func NewServiceResource() resource.Resource {
	return &ServiceResource{}
}

type ServiceResource struct {
	client *fsc.Client
}

type ServiceResourceModel struct {
	Id           types.String `tfsdk:"id"`
	Name         types.String `tfsdk:"name"`
	EndpointUrl  types.String `tfsdk:"endpoint_url"`
	InwayAddress types.String `tfsdk:"inway_address"`
}

func (r *ServiceResource) Metadata(ctx context.Context, req resource.MetadataRequest, resp *resource.MetadataResponse) {
	resp.TypeName = req.ProviderTypeName + "_service"
}

func (r *ServiceResource) Schema(ctx context.Context, req resource.SchemaRequest, resp *resource.SchemaResponse) {
	resp.Schema = schema.Schema{
		// This description is used by the documentation generator and the language server.
		MarkdownDescription: "Service resource",

		Attributes: map[string]schema.Attribute{
			"id": schema.StringAttribute{
				Computed:            true,
				MarkdownDescription: "Service Name",
				PlanModifiers: []planmodifier.String{
					stringplanmodifier.UseStateForUnknown(),
				},
			},
			"name": schema.StringAttribute{
				Required:            true,
				MarkdownDescription: "Service Name",
				PlanModifiers: []planmodifier.String{
					stringplanmodifier.RequiresReplace(),
				},
				Validators: []validator.String{
					stringvalidator.RegexMatches(
						regexp.MustCompile(`^[a-zA-Z0-9-_.]+$`),
						"must contain only letters, numbers, -, _ and .",
					),
				},
			},
			"endpoint_url": schema.StringAttribute{
				Required:            true,
				MarkdownDescription: "Endpoint URL",
				Validators: []validator.String{
					stringvalidator.LengthBetween(8, 256),
					stringvalidator.RegexMatches(
						regexp.MustCompile(`^https?://\S+$`), "must be a valid URL",
					),
				},
			},
			"inway_address": schema.StringAttribute{
				Required:            true,
				MarkdownDescription: "Inway Address",
				Validators: []validator.String{
					stringvalidator.LengthBetween(8, 256),
					stringvalidator.RegexMatches(
						regexp.MustCompile(`^https://\S+$`), "must be a valid URL",
					),
				},
			},
		},
	}
}

func (r *ServiceResource) Configure(ctx context.Context, req resource.ConfigureRequest, resp *resource.ConfigureResponse) {
	// Prevent panic if the provider has not been configured.
	if req.ProviderData == nil {
		return
	}

	client, ok := req.ProviderData.(*fsc.Client)

	if !ok {
		resp.Diagnostics.AddError(
			"Unexpected Resource Configure Type",
			fmt.Sprintf("Expected *fsc.Client, got: %T. Please report this issue to the provider developers.", req.ProviderData),
		)

		return
	}

	r.client = client
}

func (r *ServiceResource) Create(ctx context.Context, req resource.CreateRequest, resp *resource.CreateResponse) {
	var plan ServiceResourceModel

	// Read Terraform plan data into the model
	resp.Diagnostics.Append(req.Plan.Get(ctx, &plan)...)

	if resp.Diagnostics.HasError() {
		return
	}

	plan.Id = plan.Name

	service, err := r.client.GetService(plan.Name.ValueString())
	if err != nil {
		resp.Diagnostics.AddError(
			"Error creating Service",
			"Could not enumerate services: "+err.Error(),
		)
		return
	}

	if service == nil {
		err := r.client.CreateService(plan.Name.ValueString(), plan.EndpointUrl.ValueString(), plan.InwayAddress.ValueString())
		if err != nil {
			resp.Diagnostics.AddError(
				"Error creating Service",
				"Could not create service, unexpected error: "+err.Error(),
			)
			return
		}
	}

	tflog.Trace(ctx, "created a service")

	// Save data into Terraform state
	resp.Diagnostics.Append(resp.State.Set(ctx, &plan)...)
}

func (r *ServiceResource) Read(ctx context.Context, req resource.ReadRequest, resp *resource.ReadResponse) {
	var state ServiceResourceModel

	// Read Terraform prior state data into the model
	resp.Diagnostics.Append(req.State.Get(ctx, &state)...)

	if resp.Diagnostics.HasError() {
		return
	}

	service, err := r.client.GetService(state.Name.ValueString())
	if err != nil {
		resp.Diagnostics.AddError(
			"Error Reading Service",
			"Could not read service "+state.Name.ValueString()+": "+err.Error(),
		)
		return
	}

	if service == nil {
		resp.State.RemoveResource(ctx)
		return
	}

	state.EndpointUrl = types.StringValue(service.EndpointUrl)
	state.InwayAddress = types.StringValue(service.InwayAddress)

	// Save updated data into Terraform state
	resp.Diagnostics.Append(resp.State.Set(ctx, &state)...)
}

func (r *ServiceResource) Update(ctx context.Context, req resource.UpdateRequest, resp *resource.UpdateResponse) {
	var plan ServiceResourceModel

	// Read Terraform plan data into the model
	resp.Diagnostics.Append(req.Plan.Get(ctx, &plan)...)

	if resp.Diagnostics.HasError() {
		return
	}

	err := r.client.UpdateService(plan.Name.ValueString(), plan.EndpointUrl.ValueString(), plan.InwayAddress.ValueString())
	if err != nil {
		resp.Diagnostics.AddError(
			"Error Updating Service",
			"Could not update service, unexpected error: "+err.Error(),
		)
		return
	}

	// Save updated data into Terraform state
	resp.Diagnostics.Append(resp.State.Set(ctx, &plan)...)
}

func (r *ServiceResource) Delete(ctx context.Context, req resource.DeleteRequest, resp *resource.DeleteResponse) {
	var plan ServiceResourceModel

	// Read Terraform prior state data into the model
	resp.Diagnostics.Append(req.State.Get(ctx, &plan)...)

	if resp.Diagnostics.HasError() {
		return
	}

	err := r.client.DeleteService(plan.Name.ValueString())
	if err != nil {
		resp.Diagnostics.AddError(
			"Error Deleting Service",
			"Could not delete service, unexpected error: "+err.Error(),
		)
		return
	}
}

func (r *ServiceResource) ImportState(ctx context.Context, req resource.ImportStateRequest, resp *resource.ImportStateResponse) {
	resource.ImportStatePassthroughID(ctx, path.Root("name"), req, resp)
}
