package provider

import (
	"fmt"

	fsc "terraform-provider-fsc/api"

	"github.com/hashicorp/terraform-plugin-framework/attr"
	"github.com/hashicorp/terraform-plugin-framework/types"
	"github.com/hashicorp/terraform-plugin-framework/types/basetypes"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func convertPeers(peers *map[string]fsc.Peer) (*types.Map, error) {
	elements := map[string]attr.Value{}

	for _, peer := range *peers {
		element, diags := types.MapValue(types.StringType, map[string]attr.Value{
			"id":   types.StringValue(peer.Id),
			"name": types.StringValue(peer.Name),
		})
		if diags.HasError() {
			return nil, fmt.Errorf("Could not process contract peer: %s", peer)
		}

		elements[peer.Id] = element
	}

	convertedPeers, diags := types.MapValue(types.MapType{ElemType: types.StringType}, elements)
	if diags.HasError() {
		return nil, fmt.Errorf("Could not process contract peers: %s", peers)
	}
	return &convertedPeers, nil
}

func convertValidity(validity *fsc.Validity) (*types.Map, error) {
	elements := map[string]attr.Value{
		"not_before": types.Int64Value(validity.NotBefore),
		"not_after":  types.Int64Value(validity.NotAfter),
	}

	convertedValidity, diags := types.MapValue(types.Int64Type, elements)
	if diags.HasError() {
		return nil, fmt.Errorf("Could not process contract validity: %#v", validity)
	}
	return &convertedValidity, nil
}

func convertServicePublicationGrant(grant *fsc.ServicePublicationGrant, grantFields *map[string]attr.Type) (*types.Object, error) {
	directoryPeer, diags := types.MapValue(types.StringType, map[string]attr.Value{
		"peer_id": types.StringValue(grant.Directory.PeerId),
	})
	if diags.HasError() {
		return nil, fmt.Errorf("Could not process directory peer: %s", grant.Directory)
	}
	servicePublicationPeer, diags := types.MapValue(types.StringType, map[string]attr.Value{
		"peer_id":  types.StringValue(grant.Service.PeerId),
		"name":     types.StringValue(grant.Service.Name),
		"protocol": types.StringValue(string(grant.Service.Protocol)),
	})
	if diags.HasError() {
		return nil, fmt.Errorf("Could not process service publication peer: %s", grant.Service)
	}

	element := map[string]attr.Value{
		"type":      types.StringValue(string(grant.Type)),
		"hash":      types.StringValue(grant.Hash),
		"delegator": types.MapNull(types.StringType),
		"directory": directoryPeer,
		"outway":    types.MapNull(types.StringType),
		"service":   servicePublicationPeer,
	}

	convertedGrant, diags := types.ObjectValue(*grantFields, element)
	if diags.HasError() {
		return nil, fmt.Errorf("Could not process service publication grant: %s", grant)
	}

	return &convertedGrant, nil
}

func convertServiceConnectionGrant(grant *fsc.ServiceConnectionGrant, grantFields *map[string]attr.Type) (*types.Object, error) {
	outwayPeer, diags := types.MapValue(types.StringType, map[string]attr.Value{
		"public_key_thumbprint": types.StringValue(grant.Outway.PublicKeyThumbprint),
		"peer_id_":              types.StringValue(grant.Outway.PeerId),
	})
	if diags.HasError() {
		return nil, fmt.Errorf("Could not process outway peer: %s", grant.Outway)
	}

	serviceDiscriminator, err := grant.Service.Discriminator()
	if err != nil {
		return nil, fmt.Errorf("Could not determine discriminator for Service: %s", grant.Service)
	}

	var servicePeer basetypes.MapValue

	switch serviceDiscriminator {
	case string(models.SERVICETYPESERVICE):
		service, errService := grant.Service.AsService()
		if errService != nil {
			return nil, fmt.Errorf("Could not determine service: %s", grant.Service)
		}

		servicePeer, diags = types.MapValue(types.StringType, map[string]attr.Value{
			"peer_id": types.StringValue(service.Peer.Id),
			"name":    types.StringValue(service.Name),
		})
		if diags.HasError() {
			return nil, fmt.Errorf("Could not process service publication peer: %s", grant.Service)
		}

	case string(models.SERVICETYPEDELEGATEDSERVICE):
		service, errService := grant.Service.AsDelegatedService()
		if errService != nil {
			return nil, fmt.Errorf("Could not determine service: %s", grant.Service)
		}

		servicePeer, diags = types.MapValue(types.StringType, map[string]attr.Value{
			"delegator_peer_id": types.StringValue(service.Delegator.Id),
			"peer_id":           types.StringValue(service.Peer.Id),
			"name":              types.StringValue(service.Name),
		})
		if diags.HasError() {
			return nil, fmt.Errorf("Could not process service publication peer: %s", grant.Service)
		}
	}

	element := map[string]attr.Value{
		"type":      types.StringValue(string(grant.Type)),
		"hash":      types.StringValue(grant.Hash),
		"delegator": types.MapNull(types.StringType),
		"directory": types.MapNull(types.StringType),
		"outway":    outwayPeer,
		"service":   servicePeer,
	}

	convertedGrant, diags := types.ObjectValue(*grantFields, element)
	if diags.HasError() {
		return nil, fmt.Errorf("Could not process service connection grant: %s\n%s", grant, diags[0].Detail())
	}

	return &convertedGrant, nil
}

func convertDelegatedServicePublicationGrant(grant *fsc.DelegatedServicePublicationGrant, grantFields *map[string]attr.Type) (*types.Object, error) {
	delegatorPeer, diags := types.MapValue(types.StringType, map[string]attr.Value{
		"peer_id": types.StringValue(grant.Delegator.PeerId),
	})
	if diags.HasError() {
		return nil, fmt.Errorf("Could not process directory peer: %s", grant.Delegator)
	}
	directoryPeer, diags := types.MapValue(types.StringType, map[string]attr.Value{
		"peer_id": types.StringValue(grant.Directory.PeerId),
	})
	if diags.HasError() {
		return nil, fmt.Errorf("Could not process directory peer: %s", grant.Directory)
	}
	servicePublicationPeer, diags := types.MapValue(types.StringType, map[string]attr.Value{
		"peer_id":  types.StringValue(grant.Service.PeerId),
		"name":     types.StringValue(grant.Service.Name),
		"protocol": types.StringValue(string(grant.Service.Protocol)),
	})
	if diags.HasError() {
		return nil, fmt.Errorf("Could not process service publication peer: %s", grant.Service)
	}

	value := map[string]attr.Value{
		"type":      types.StringValue(string(grant.Type)),
		"hash":      types.StringValue(grant.Hash),
		"delegator": delegatorPeer,
		"directory": directoryPeer,
		"outway":    types.MapNull(types.StringType),
		"service":   servicePublicationPeer,
	}

	convertedGrant, diags := types.ObjectValue(*grantFields, value)
	if diags.HasError() {
		return nil, fmt.Errorf("Could not process delegated service publication grant: %s", grant)
	}

	return &convertedGrant, nil
}

func convertDelegatedServiceConnectionGrant(grant *fsc.DelegatedServiceConnectionGrant, grantFields *map[string]attr.Type) (*types.Object, error) {
	delegatorPeer, diags := types.MapValue(types.StringType, map[string]attr.Value{
		"peer_id": types.StringValue(grant.Delegator.PeerId),
	})
	if diags.HasError() {
		return nil, fmt.Errorf("Could not process directory peer: %s", grant.Delegator)
	}
	outwayPeer, diags := types.MapValue(types.StringType, map[string]attr.Value{
		"public_key_thumbprint": types.StringValue(grant.Outway.PublicKeyThumbprint),
		"peer_id_":              types.StringValue(grant.Outway.PeerId),
	})
	if diags.HasError() {
		return nil, fmt.Errorf("Could not process outway peer: %s", grant.Outway)
	}

	serviceDiscriminator, err := grant.Service.Discriminator()
	if err != nil {
		return nil, fmt.Errorf("Could not determine discriminator for Service: %s", grant.Service)
	}

	var servicePeer basetypes.MapValue

	switch serviceDiscriminator {
	case string(models.SERVICETYPESERVICE):
		service, errService := grant.Service.AsService()
		if errService != nil {
			return nil, fmt.Errorf("Could not determine service: %s", grant.Service)
		}

		servicePeer, diags = types.MapValue(types.StringType, map[string]attr.Value{
			"peer_id": types.StringValue(service.Peer.Id),
			"name":    types.StringValue(service.Name),
		})
		if diags.HasError() {
			return nil, fmt.Errorf("Could not process service publication peer: %s", grant.Service)
		}

	case string(models.SERVICETYPEDELEGATEDSERVICE):
		service, errService := grant.Service.AsDelegatedService()
		if errService != nil {
			return nil, fmt.Errorf("Could not determine service: %s", grant.Service)
		}

		servicePeer, diags = types.MapValue(types.StringType, map[string]attr.Value{
			"delegator_peer_id": types.StringValue(service.Delegator.Id),
			"peer_id":           types.StringValue(service.Peer.Id),
			"name":              types.StringValue(service.Name),
		})
		if diags.HasError() {
			return nil, fmt.Errorf("Could not process service publication peer: %s", grant.Service)
		}
	}

	value := map[string]attr.Value{
		"type":      types.StringValue(string(grant.Type)),
		"hash":      types.StringValue(grant.Hash),
		"delegator": delegatorPeer,
		"directory": types.MapNull(types.StringType),
		"outway":    outwayPeer,
		"service":   servicePeer,
	}

	convertedGrant, diags := types.ObjectValue(*grantFields, value)
	if diags.HasError() {
		return nil, fmt.Errorf("Could not process delegated service connection grant: %s", grant)
	}

	return &convertedGrant, nil
}

func convertGrants(grants *fsc.Grants, grantFields *map[string]attr.Type) (*types.List, error) {
	var elements []attr.Value

	for _, grant := range *grants {
		discriminator, err := grant.ValueByDiscriminator()
		if err != nil {
			return nil, fmt.Errorf("Could not process grant discriminator: %s", err)
		}

		switch decodedGrant := discriminator.(type) {
		case fsc.ServicePublicationGrant:
			convertedGrant, err := convertServicePublicationGrant(&decodedGrant, grantFields)
			if err != nil {
				return nil, err
			}
			elements = append(elements, convertedGrant)
		case fsc.ServiceConnectionGrant:
			convertedGrant, err := convertServiceConnectionGrant(&decodedGrant, grantFields)
			if err != nil {
				return nil, err
			}
			elements = append(elements, convertedGrant)
		case fsc.DelegatedServicePublicationGrant:
			convertedGrant, err := convertDelegatedServicePublicationGrant(&decodedGrant, grantFields)
			if err != nil {
				return nil, err
			}
			elements = append(elements, convertedGrant)
		case fsc.DelegatedServiceConnectionGrant:
			convertedGrant, err := convertDelegatedServiceConnectionGrant(&decodedGrant, grantFields)
			if err != nil {
				return nil, err
			}
			elements = append(elements, convertedGrant)
		}
	}

	convertedGrants, diags := types.ListValue(types.ObjectType{AttrTypes: *grantFields}, elements)
	if diags.HasError() {
		return nil, fmt.Errorf("Could not process grants: %s\n%s", grants, diags[0].Detail())
	}
	return &convertedGrants, nil
}

func convertContent(content *fsc.ContractContent) (*types.Object, error) {
	grantFields := map[string]attr.Type{
		"type":      types.StringType,
		"hash":      types.StringType,
		"delegator": types.MapType{ElemType: types.StringType},
		"directory": types.MapType{ElemType: types.StringType},
		"outway":    types.MapType{ElemType: types.StringType},
		"service":   types.MapType{ElemType: types.StringType},
	}

	contentFields := map[string]attr.Type{
		"id":             types.StringType,
		"group_id":       types.StringType,
		"validity":       types.MapType{ElemType: types.Int64Type},
		"grants":         types.ListType{ElemType: types.ObjectType{AttrTypes: grantFields}},
		"hash_algorithm": types.StringType,
		"created_at":     types.Int64Type,
	}

	validity, err := convertValidity(&content.Validity)
	if err != nil {
		return nil, err
	}

	grants, err := convertGrants(&content.Grants, &grantFields)
	if err != nil {
		return nil, err
	}

	elements := map[string]attr.Value{
		"iv":             types.StringValue(content.Iv),
		"group_id":       types.StringValue(content.GroupId),
		"validity":       validity,
		"grants":         grants,
		"hash_algorithm": types.StringValue(string(content.HashAlgorithm)),
		"created_at":     types.Int64Value(content.CreatedAt),
	}

	convertedContent, diags := types.ObjectValue(contentFields, elements)
	if diags.HasError() {
		return nil, fmt.Errorf("Could not process contract content: %s", diags[0].Detail())
	}

	return &convertedContent, nil
}

func convertSignatureMap(signatureMap *fsc.SignatureMap, mapFields *map[string]attr.Type) (*types.Map, error) {
	elements := map[string]attr.Value{}

	for peerId, signature := range *signatureMap {
		signerInfo, diags := types.MapValue(types.StringType, map[string]attr.Value{
			"id":   types.StringValue(signature.Peer.Id),
			"name": types.StringValue(signature.Peer.Name),
		})
		if diags.HasError() {
			return nil, fmt.Errorf("Could not process signer details: %s", signature.Peer)
		}

		element, diags := types.ObjectValue(*mapFields, map[string]attr.Value{
			"peer":      signerInfo,
			"signed_at": types.Int64Value(signature.SignedAt),
		})
		if diags.HasError() {
			return nil, fmt.Errorf("Could not process signature details: %#v", signature)
		}

		elements[peerId] = element
	}

	convertedSignatureMap, diags := types.MapValue(types.ObjectType{AttrTypes: *mapFields}, elements)
	if diags.HasError() {
		return nil, fmt.Errorf("Could not process contract signature: %#v", signatureMap)
	}

	return &convertedSignatureMap, nil
}

func convertSignatures(signatures *fsc.Signatures) (*types.Object, error) {
	mapFields := map[string]attr.Type{
		"peer":      types.MapType{ElemType: types.StringType},
		"signed_at": types.Int64Type,
	}

	acceptSignatures, err := convertSignatureMap(&signatures.Accept, &mapFields)
	if err != nil {
		return nil, err
	}

	rejectSignatures, err := convertSignatureMap(&signatures.Reject, &mapFields)
	if err != nil {
		return nil, err
	}

	revokeSignatures, err := convertSignatureMap(&signatures.Revoke, &mapFields)
	if err != nil {
		return nil, err
	}

	fields := map[string]attr.Type{
		"accept": types.MapType{ElemType: types.ObjectType{AttrTypes: mapFields}},
		"reject": types.MapType{ElemType: types.ObjectType{AttrTypes: mapFields}},
		"revoke": types.MapType{ElemType: types.ObjectType{AttrTypes: mapFields}},
	}

	elements := map[string]attr.Value{
		"accept": acceptSignatures,
		"reject": rejectSignatures,
		"revoke": revokeSignatures,
	}

	convertedSignatures, diags := types.ObjectValue(fields, elements)
	if diags.HasError() {
		return nil, fmt.Errorf("Could not process signatures: %#v", signatures)
	}

	return &convertedSignatures, nil
}

func convertContract(contract *fsc.Contract) (*ContractDataSourceModel, error) {
	convertedPeers, err := convertPeers(&contract.Peers)
	if err != nil {
		return nil, err
	}

	convertedContent, err := convertContent(&contract.Content)
	if err != nil {
		return nil, err
	}

	convertedSignatures, err := convertSignatures(&contract.Signatures)
	if err != nil {
		return nil, err
	}

	convertedContract := &ContractDataSourceModel{
		ContentHash: types.StringValue(contract.Hash),
		Peers:       *convertedPeers,
		Content:     *convertedContent,
		HasRejected: types.BoolValue(contract.HasRejected),
		HasRevoked:  types.BoolValue(contract.HasRevoked),
		HasAccepted: types.BoolValue(contract.HasAccepted),
		Signatures:  *convertedSignatures,
	}

	return convertedContract, nil
}
