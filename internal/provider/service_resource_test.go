// Copyright (c) HashiCorp, Inc.
// SPDX-License-Identifier: MPL-2.0

package provider

import (
	"fmt"
	"testing"

	"github.com/hashicorp/terraform-plugin-testing/helper/resource"
)

func TestAccServiceResource(t *testing.T) {
	resource.Test(t, resource.TestCase{
		PreCheck:                 func() { testAccPreCheck(t) },
		ProtoV6ProviderFactories: testAccProtoV6ProviderFactories,
		Steps: []resource.TestStep{
			// Create and Read testing
			{
				Config: testAccServiceResourceConfig("service_foo", "https://endpoint_foo:443", "https://inway_address:443"),
				Check: resource.ComposeAggregateTestCheckFunc(
					resource.TestCheckResourceAttr("fsc_service.test", "name", "service_foo"),
					resource.TestCheckResourceAttr("fsc_service.test", "endpoint_url", "https://endpoint_foo:443"),
					resource.TestCheckResourceAttr("fsc_service.test", "inway_address", "https://inway_address:443"),
				),
			},
			// ImportState testing
			{
				ResourceName:                         "fsc_service.test",
				ImportState:                          true,
				ImportStateVerify:                    true,
				ImportStateVerifyIgnore:              []string{"id"},
				ImportStateVerifyIdentifierAttribute: "name",
			},
			// Update and Read testing
			{
				Config: testAccServiceResourceConfig("service_bar", "https://endpoint_bar:443", "https://inway_address:443"),
				Check: resource.ComposeAggregateTestCheckFunc(
					resource.TestCheckResourceAttr("fsc_service.test", "name", "service_bar"),
					resource.TestCheckResourceAttr("fsc_service.test", "endpoint_url", "https://endpoint_bar:443"),
					resource.TestCheckResourceAttr("fsc_service.test", "inway_address", "https://inway_address:443"),
				),
			},
			// Delete testing automatically occurs in TestCase
		},
	})
}

func testAccServiceResourceConfig(name, endpointUrl, inwayAddress string) string {
	return fmt.Sprintf(`
resource "fsc_service" "test" {
  name          = %[1]q
  endpoint_url  = %[2]q
  inway_address = %[3]q
}
`, name, endpointUrl, inwayAddress)
}
