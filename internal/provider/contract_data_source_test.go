// Copyright (c) HashiCorp, Inc.
// SPDX-License-Identifier: MPL-2.0

package provider

import (
	"testing"

	_ "github.com/hashicorp/terraform-plugin-testing/helper/resource"
)

func TestAccContractDataSource(t *testing.T) {
	// TODO: add test that verifies contract enumeration
	// See https://github.com/hashicorp/terraform-plugin-testing/issues/85
}

const testAccContractDataSourceConfig = `
  TODO
`
