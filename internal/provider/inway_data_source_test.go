// Copyright (c) HashiCorp, Inc.
// SPDX-License-Identifier: MPL-2.0

package provider

import (
	"fmt"
	"regexp"
	"testing"

	"github.com/hashicorp/terraform-plugin-testing/helper/resource"
)

func TestAccInwayDataSource(t *testing.T) {
	inways, err := apiClient.GetInways()
	if err != nil || len(inways) == 0 {
		t.Logf("Failed to find any registered Inway")
		t.FailNow()
	}

	inwayName := inways[0].Name
	notEmpty := regexp.MustCompile(`^.+$`)

	resource.Test(t, resource.TestCase{
		PreCheck:                 func() { testAccFscClientCheck(t) },
		ProtoV6ProviderFactories: testAccProtoV6ProviderFactories,
		Steps: []resource.TestStep{
			// Read testing
			{
				Config: testAccInwayDataResourceConfig(inwayName),
				Check: resource.ComposeAggregateTestCheckFunc(
					resource.TestCheckResourceAttr("data.fsc_inway.foo", "name", inwayName),
					resource.TestMatchResourceAttr("data.fsc_inway.foo", "address", notEmpty),
				),
			},
		},
	})
}

func testAccInwayDataResourceConfig(name string) string {
	return fmt.Sprintf(`
data "fsc_inway" "foo" {
  name          = %[1]q
}`, name)
}
