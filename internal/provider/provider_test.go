// Copyright (c) HashiCorp, Inc.
// SPDX-License-Identifier: MPL-2.0

package provider

import (
	"log"
	"os"
	"strings"
	"testing"

	fsc "terraform-provider-fsc/api"

	"github.com/hashicorp/terraform-plugin-framework/providerserver"
	"github.com/hashicorp/terraform-plugin-go/tfprotov6"
)

// testAccProtoV6ProviderFactories are used to instantiate a provider during
// acceptance testing. The factory function will be invoked for every Terraform
// CLI command executed to create a provider server to which the CLI can
// reattach.
var testAccProtoV6ProviderFactories = map[string]func() (tfprotov6.ProviderServer, error){
	"fsc": providerserver.NewProtocol6WithError(New("test")()),
}

const (
	FSC_GROUP_ID                    = "FSC_GROUP_ID"
	FSC_CONTROLLER_ENDPOINT         = "FSC_CONTROLLER_ENDPOINT"
	FSC_MANAGER_ENDPOINT            = "FSC_MANAGER_ENDPOINT"
	FSC_CONTROLLER_CLIENT_CERT      = "FSC_CONTROLLER_CLIENT_CERT"
	FSC_CONTROLLER_CLIENT_CERT_PATH = "FSC_CONTROLLER_CLIENT_CERT_PATH"
	FSC_CONTROLLER_CLIENT_KEY       = "FSC_CONTROLLER_CLIENT_KEY"
	FSC_CONTROLLER_CLIENT_KEY_PATH  = "FSC_CONTROLLER_CLIENT_KEY_PATH"
	FSC_MANAGER_CLIENT_CERT         = "FSC_MANAGER_CLIENT_CERT"
	FSC_MANAGER_CLIENT_CERT_PATH    = "FSC_MANAGER_CLIENT_CERT_PATH"
	FSC_MANAGER_CLIENT_KEY          = "FSC_MANAGER_CLIENT_KEY"
	FSC_MANAGER_CLIENT_KEY_PATH     = "FSC_MANAGER_CLIENT_KEY_PATH"
	FSC_CA_CERTS                    = "FSC_CA_CERTS"
	FSC_CA_CERTS_PATH               = "FSC_CA_CERTS_PATH"
)

var (
	groupId                  = os.Getenv(FSC_GROUP_ID)
	controllerEndpoint       = os.Getenv(FSC_CONTROLLER_ENDPOINT)
	managerEndpoint          = os.Getenv(FSC_MANAGER_ENDPOINT)
	caCertsPath              = os.Getenv(FSC_CA_CERTS_PATH)
	controllerClientCertPath = os.Getenv(FSC_CONTROLLER_CLIENT_CERT_PATH)
	controllerClientKeyPath  = os.Getenv(FSC_CONTROLLER_CLIENT_KEY_PATH)
	managerClientCertPath    = os.Getenv(FSC_MANAGER_CLIENT_CERT_PATH)
	managerClientKeyPath     = os.Getenv(FSC_MANAGER_CLIENT_KEY_PATH)

	apiClient    *fsc.Client
	apiClientErr error
)

func init() {

	caCertsPem := resolvePEMvars(FSC_CA_CERTS, caCertsPath)
	controllerClientCertPem := resolvePEMvars(FSC_CONTROLLER_CLIENT_CERT, controllerClientCertPath)
	controllerClientKeyPem := resolvePEMvars(FSC_CONTROLLER_CLIENT_KEY, controllerClientKeyPath)
	managerClientCertPem := resolvePEMvars(FSC_MANAGER_CLIENT_CERT, managerClientCertPath)
	managerClientKeyPem := resolvePEMvars(FSC_MANAGER_CLIENT_KEY, managerClientKeyPath)

	client, err := fsc.NewClient(groupId, controllerEndpoint, managerEndpoint, []string{caCertsPem, controllerClientCertPem, controllerClientKeyPem}, []string{caCertsPem, managerClientCertPem, managerClientKeyPem})
	if err != nil {
		log.Fatalf("could not create clients for Manager and Controller: %v", err)
	}
	apiClient = client
	apiClientErr = err
}

func resolvePEMvars(envVar, path string) string {
	pem := os.Getenv(envVar)
	if strings.HasPrefix(pem, "-----BEGIN") {
		return pem
	}

	if path == "" {
		log.Fatal("need to either provide a path or a PEM file in the environment variables")
	}

	pemFile, err := os.ReadFile(path)
	if err != nil {
		log.Fatalf("could not read PEM from file: %v", err)
	}

	err = os.Setenv(envVar, string(pemFile))
	if err != nil {
		log.Fatalf("could to set Env var: %s with PEM file from path: %s", envVar, path)
	}

	return string(pemFile)
}

func testAccPreCheck(t *testing.T) {
	// You can add code here to run prior to any test case execution, for example assertions
	// about the appropriate environment variables being set are common to see in a pre-check
	// function.
}

func testAccFscClientCheck(t *testing.T) {
	if apiClientErr != nil {
		t.Logf("Failed to create FSC API client: %s", apiClientErr)
		t.FailNow()
	}
	return
}
