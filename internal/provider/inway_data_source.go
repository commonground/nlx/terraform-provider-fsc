// Copyright (c) HashiCorp, Inc.
// SPDX-License-Identifier: MPL-2.0

package provider

import (
	"context"
	"fmt"

	"github.com/hashicorp/terraform-plugin-framework/datasource"
	"github.com/hashicorp/terraform-plugin-framework/datasource/schema"
	"github.com/hashicorp/terraform-plugin-framework/types"
	"github.com/hashicorp/terraform-plugin-log/tflog"
	fsc "terraform-provider-fsc/api"
)

var _ datasource.DataSource = &InwayDataSource{}

func NewInwayDataSource() datasource.DataSource {
	return &InwayDataSource{}
}

type InwayDataSource struct {
	client *fsc.Client
}

type InwayDataSourceModel struct {
	Name    types.String `tfsdk:"name"`
	Address types.String `tfsdk:"address"`
}

func (d *InwayDataSource) Metadata(ctx context.Context, req datasource.MetadataRequest, resp *datasource.MetadataResponse) {
	resp.TypeName = req.ProviderTypeName + "_inway"
}

func (d *InwayDataSource) Schema(ctx context.Context, req datasource.SchemaRequest, resp *datasource.SchemaResponse) {
	resp.Schema = schema.Schema{
		// This description is used by the documentation generator and the language server.
		MarkdownDescription: "Fetches a FSC Inway",

		Attributes: map[string]schema.Attribute{
			"name": schema.StringAttribute{
				MarkdownDescription: "Name of FSC inway to fetch",
				Required:            true,
			},
			"address": schema.StringAttribute{
				MarkdownDescription: "Address of FSC inway to fetch",
				Computed:            true,
			},
		},
	}
}

func (d *InwayDataSource) Configure(ctx context.Context, req datasource.ConfigureRequest, resp *datasource.ConfigureResponse) {
	// Prevent panic if the provider has not been configured.
	if req.ProviderData == nil {
		return
	}

	client, ok := req.ProviderData.(*fsc.Client)

	if !ok {
		resp.Diagnostics.AddError(
			"Unexpected Data Source Configure Type",
			fmt.Sprintf("Expected *fsc.Client, got: %T. Please report this issue to the provider developers.", req.ProviderData),
		)

		return
	}

	d.client = client
}

func (d *InwayDataSource) Read(ctx context.Context, req datasource.ReadRequest, resp *datasource.ReadResponse) {
	var plan, state InwayDataSourceModel

	// Read Terraform configuration data into the model
	resp.Diagnostics.Append(req.Config.Get(ctx, &plan)...)
	resp.Diagnostics.Append(resp.State.Get(ctx, &state)...)

	if resp.Diagnostics.HasError() {
		return
	}

	inway, err := d.client.GetInway(plan.Name.ValueString())
	if err != nil {
		resp.Diagnostics.AddError(
			"Error retrieving Inway",
			"Could not enumerate inways: "+err.Error(),
		)
		return
	}

	if inway == nil {
		resp.Diagnostics.AddError(
			"Error retrieving Inway",
			"Could not find a matching Inway",
		)
		return
	}

	state.Name = types.StringValue(inway.Name)
	state.Address = types.StringValue(inway.Address)

	tflog.Trace(ctx, "read an inway data source")

	// Save data into Terraform state
	resp.Diagnostics.Append(resp.State.Set(ctx, &state)...)
}
