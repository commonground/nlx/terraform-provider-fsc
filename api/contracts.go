package fsc

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"
)

func (c *Client) CreateContract(contractId, algorithm string, notBefore, notAfter int64, grants []Grant) error {
	request := CreateContractJSONBody{
		ContractContent: ContractContent{
			Iv:      contractId,
			GroupId: c.GroupId,
			Validity: Validity{
				NotBefore: notBefore,
				NotAfter:  notAfter,
			},
			Grants:        grants,
			HashAlgorithm: HashAlgorithm("HASH_ALGORITHM_" + strings.ToUpper(algorithm)),
			CreatedAt:     time.Now().Unix(),
		},
	}

	body, err := json.Marshal(request)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("POST", fmt.Sprintf("%s/contracts", c.ManagerEndpoint), bytes.NewBuffer(body))
	if err != nil {
		return err
	}

	req.Header.Set("content-type", "application/json")

	_, err = c.doManagerRequest(req)
	if err != nil {
		return err
	}

	return nil
}

func (c *Client) GetContracts() ([]Contract, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s/contracts", c.ManagerEndpoint), nil)
	if err != nil {
		return nil, err
	}

	body, err := c.doManagerRequest(req)
	if err != nil {
		return nil, err
	}

	response := GetContractsResponse{}
	err = json.Unmarshal(body, &response)
	if err != nil {
		return nil, err
	}

	return response.Contracts, nil
}

func (c *Client) GetContract(contractId string) (*Contract, error) {
	contracts, err := c.GetContracts()
	if err != nil {
		return nil, err
	}

	for _, contract := range contracts {
		if contract.Content.Iv == contractId {
			return &contract, nil
		}
	}

	return nil, nil
}

func (c *Client) RevokeContract(contractHash string) error {
	req, err := http.NewRequest("PUT", fmt.Sprintf("%s/contracts/%s/revoke", c.ManagerEndpoint, contractHash), nil)
	if err != nil {
		return err
	}

	_, err = c.doManagerRequest(req)
	if err != nil {
		return err
	}

	return nil
}
