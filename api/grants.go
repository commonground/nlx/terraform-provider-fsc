package fsc

import "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"

func GrantServicePublication(directoryPeerId, servicePeerId, serviceName, serviceProtocol string) Grant {
	var grant Grant
	grant.FromGrantServicePublication(ServicePublicationGrant{
		Directory: DirectoryPeer{
			PeerId: directoryPeerId,
		},
		Service: ServicePublicationPeer{
			Name:     serviceName,
			PeerId:   servicePeerId,
			Protocol: Protocol(serviceProtocol),
		},
	})

	return grant
}

func GrantServiceConnection(publicKeyThumbprint, outwayPeerId, servicePeerId, serviceDelegatorPeerId, serviceName string) Grant {
	service := models.GrantServiceConnection_Service{}

	if serviceDelegatorPeerId != "" {
		service.FromDelegatedService(models.DelegatedService{
			Delegator: models.Peer{
				Id: serviceDelegatorPeerId,
			},
			Name: serviceName,
			Peer: models.Peer{
				Id: servicePeerId,
			},
			Type: models.SERVICETYPEDELEGATEDSERVICE,
		})
	} else {
		service.FromService(models.Service{
			Name: serviceName,
			Peer: models.Peer{
				Id: servicePeerId,
			},
			Type: models.SERVICETYPESERVICE,
		})
	}

	var grant Grant
	grant.FromGrantServiceConnection(ServiceConnectionGrant{
		Outway: OutwayPeer{
			PublicKeyThumbprint: publicKeyThumbprint,
			PeerId:              outwayPeerId,
		},
		Service: service,
	})

	return grant
}

func GrantDelegatedServicePublication(delegatorPeerId, directoryPeerId, servicePeerId, serviceName, serviceProtocol string) Grant {
	var grant Grant
	grant.FromGrantDelegatedServicePublication(DelegatedServicePublicationGrant{
		Delegator: DelegatorPeer{
			PeerId: delegatorPeerId,
		},
		Directory: DirectoryPeer{
			PeerId: directoryPeerId,
		},
		Service: ServicePublicationPeer{
			Name:     serviceName,
			PeerId:   servicePeerId,
			Protocol: Protocol(serviceProtocol),
		},
	})

	return grant
}

func GrantDelegatedServiceConnection(delegatorPeerId, publicKeyThumbprint, outwayPeerId, servicePeerId, serviceDelegatorPeerId, serviceName string) Grant {
	service := models.GrantDelegatedServiceConnection_Service{}

	if serviceDelegatorPeerId != "" {
		service.FromDelegatedService(models.DelegatedService{
			Delegator: models.Peer{
				Id: serviceDelegatorPeerId,
			},
			Name: serviceName,
			Peer: models.Peer{
				Id: servicePeerId,
			},
			Type: models.SERVICETYPEDELEGATEDSERVICE,
		})
	} else {
		service.FromService(models.Service{
			Name: serviceName,
			Peer: models.Peer{
				Id: servicePeerId,
			},
			Type: models.SERVICETYPESERVICE,
		})
	}

	var grant Grant
	grant.FromGrantDelegatedServiceConnection(DelegatedServiceConnectionGrant{
		Delegator: DelegatorPeer{
			PeerId: delegatorPeerId,
		},
		Outway: OutwayPeer{
			PublicKeyThumbprint: publicKeyThumbprint,
			PeerId:              outwayPeerId,
		},
		Service: service,
	})

	return grant
}
