package fsc

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"slices"
	"time"
)

type Client struct {
	ControllerEndpoint   string
	ManagerEndpoint      string
	HTTPClientController *http.Client
	HTTPClientManager    *http.Client
	GroupId              string
}

type Certs struct {
	CACerts    *x509.CertPool
	ClientCert tls.Certificate
}

func NewClient(groupId, controllerEndpoint, managerEndpoint string, controllerTlsConfig, managerTlsConfig []string) (*Client, error) {
	controllerConfig, err := loadKeyMaterial(controllerTlsConfig)
	if err != nil {
		return nil, err
	}

	managerConfig, err := loadKeyMaterial(managerTlsConfig)
	if err != nil {
		return nil, err
	}

	c := Client{
		HTTPClientController: &http.Client{
			Timeout: 10 * time.Second,
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					RootCAs:      controllerConfig.CACerts,
					Certificates: []tls.Certificate{controllerConfig.ClientCert},
				},
			},
		},
		HTTPClientManager: &http.Client{
			Timeout: 10 * time.Second,
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					RootCAs:      managerConfig.CACerts,
					Certificates: []tls.Certificate{managerConfig.ClientCert},
				},
			},
		},
		GroupId:            groupId,
		ControllerEndpoint: controllerEndpoint,
		ManagerEndpoint:    managerEndpoint,
	}

	return &c, nil
}

func loadKeyMaterial(keys []string) (*Certs, error) {
	if len(keys) != 3 {
		return nil, fmt.Errorf("FSC-client requires CA certificates, a client certificate and a private key")
	}

	var (
		CACertsPem    = []byte(keys[0])
		clientCertPem = []byte(keys[1])
		clientKeyPem  = []byte(keys[2])
	)

	clientCert, err := tls.X509KeyPair(clientCertPem, clientKeyPem)
	if err != nil {
		return nil, err
	}

	CACertPool := x509.NewCertPool()
	ok := CACertPool.AppendCertsFromPEM(CACertsPem)
	if !ok {
		return nil, fmt.Errorf("failed to load CA certificate(s)")
	}

	return &Certs{
		CACerts:    CACertPool,
		ClientCert: clientCert,
	}, nil
}

func (c *Client) doControllerRequest(req *http.Request) ([]byte, error) {
	body, err := c.doRequest(c.HTTPClientController, req)
	if err != nil {
		return nil, err
	}

	return body, err
}

func (c *Client) doManagerRequest(req *http.Request) ([]byte, error) {
	body, err := c.doRequest(c.HTTPClientManager, req)
	if err != nil {
		return nil, err
	}

	return body, err
}

func (c *Client) doRequest(client *http.Client, req *http.Request) ([]byte, error) {
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	ok := []int{http.StatusOK, http.StatusNoContent, http.StatusCreated}
	if !slices.Contains(ok, res.StatusCode) {
		return nil, fmt.Errorf("status: %d, body: %s", res.StatusCode, body)
	}

	return body, err
}

func (c *Client) GetPeerInfo() (*Peer, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s/peer", c.ManagerEndpoint), nil)
	if err != nil {
		return nil, err
	}

	body, err := c.doManagerRequest(req)
	if err != nil {
		return nil, err
	}

	response := GetPeerInfoResponse{}
	err = json.Unmarshal(body, &response)
	if err != nil {
		return nil, err
	}

	return &response.Peer, nil
}
