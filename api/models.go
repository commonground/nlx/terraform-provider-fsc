package fsc

import (
	controller_models "gitlab.com/commonground/nlx/fsc-nlx/controller/ports/rest/api/models"
	manager_models "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

type HashAlgorithm = manager_models.HashAlgorithm

const (
	HashAlgorithmSHA3512 = manager_models.HASHALGORITHMSHA3512
)

type CertificateThumbprint = manager_models.CertificateThumbprint
type Protocol = manager_models.Protocol

type Peer = manager_models.Peer
type PeerId = manager_models.PeerID
type OutwayPeer = manager_models.OutwayPeer
type DirectoryPeer = manager_models.DirectoryPeer
type DelegatorPeer = manager_models.DelegatorPeer
type ServicePublicationPeer = manager_models.ServicePublicationPeer

type ServicePublicationGrant = manager_models.GrantServicePublication
type ServiceConnectionGrant = manager_models.GrantServiceConnection
type DelegatedServicePublicationGrant = manager_models.GrantDelegatedServicePublication
type DelegatedServiceConnectionGrant = manager_models.GrantDelegatedServiceConnection

const (
	ServicePublicationGrantType          = string(manager_models.GRANTTYPESERVICEPUBLICATION)
	ServiceConnectionGrantType           = string(manager_models.GRANTTYPESERVICECONNECTION)
	DelegatedServicePublicationGrantType = string(manager_models.GRANTTYPEDELEGATEDSERVICEPUBLICATION)
	DelegatedServiceConnectionGrantType  = string(manager_models.GRANTTYPEDELEGATEDSERVICECONNECTION)
)

type Grants = []manager_models.Grant
type Grant = manager_models.Grant
type Validity = manager_models.Validity
type SignatureMap = manager_models.SignatureMap
type Signatures = manager_models.Signatures

type Service = controller_models.Service
type ContractContent = manager_models.ContractContent
type CreateContractJSONBody = manager_models.CreateContractJSONBody
type Contract = manager_models.Contract
type Outway = controller_models.Outway
type Inway = controller_models.Inway

type GetServicesResponse struct {
	Services []controller_models.Service `json:"services"`
}

type GetContractsResponse struct {
	Contracts []manager_models.Contract `json:"contracts"`
}

type GetOutwaysResponse struct {
	Outways []controller_models.Outway `json:"outways"`
}

type GetInwaysResponse struct {
	Inways []controller_models.Inway `json:"inways"`
}

type GetPeerInfoResponse struct {
	Peer Peer `json:"peer"`
}
